<?php

/**
* $file
* Add a Term Hierarchy plugin to the feeds tamper module.
* Allows feed importers to import hierarchical taxonomy fields.
* Supports auto-creation of new terms.
*/

$plugin = [
		'form' => 'feeds_tamper_term_hierarchy_form',
			'callback' => 'feeds_tamper_term_hierarchy_callback',
				'name' => 'Taxonomy Term Hierarchy',
					'multi' => 'direct',
						'category' => 'List',
					];

					/**
					 * Standard Feeds Tamper plugin form function.
					 */
					 function feeds_tamper_term_hierarchy_form( $importer, $element_key, $settings ) {
						 	// we may need to manually include the Feeds taxonomy mapper so that we have access to its constants
						 	if ( !defined( 'FEEDS_TAXONOMY_SEARCH_TERM_NAME' ) ) {
										require_once dirname(__FILE__) . '/../../feeds/mappers/taxonomy.inc';
											}
												foreach ( $importer->processor->config[ 'mappings' ] AS $mapping ) {
															if ( $mapping[ 'source' ] == $element_key ) {
																			// warn them that their current mapping settings are incompatible with this plugin.
																						if ( !isset( $mapping[ 'term_search' ] ) || (int) $mapping[ 'term_search' ] !== FEEDS_TAXONOMY_SEARCH_TERM_NAME ) {
																											drupal_set_message( t( 'The Feeds Tamper Term Hierarchy plugin requires that terms be searched by <b>Term name</b>. To use this plugin, change this field\'s target configuration to <i>Search taxonomy terms by <b>Term name</b></i> in your <a href="@url">mappings</a>.', [ '@url' => url( 'admin/structure/feeds/' . $importer->id . '/mapping' ) ] ), 'warning' );
																														}
																																	break;
																																			}
																																				}
																																					return [
																																								'separator' => [
																																												'#type' => 'textfield',
																																															'#title' => t( 'Term hierarchy separator' ),
																																																		'#default_value' => isset( $settings[ 'separator' ] ) ? $settings[ 'separator' ] : '>>',
																																																					'#description' => t( 'Define the separator used to separate terms levels.' ),
																																																								'#required' => true,
																																																										]
																																																											];  
																																																										}

																																																										/**
																																																										 * Standard Feeds Tamper plugin callback function.
																																																										  */
																																																										  function feeds_tamper_term_hierarchy_callback( $feeds_parser_result, $item_key, $element_key, &$field, $settings, $feeds_source ) {
																																																											  	// find the mapping settings
																																																													foreach ( $feeds_source->importer->processor->config[ 'mappings' ] AS $mapping ) {
																																																																if ( $mapping[ 'source' ] == $element_key ) {
																																																																				// ensure that we are searching terms by name
																																																																							if ( !isset( $mapping[ 'term_search' ] ) || (int) $mapping[ 'term_search' ] !== FEEDS_TAXONOMY_SEARCH_TERM_NAME ) {
																																																																												watchdog( 'Feeds Tamper Term Hierarchy', 'The Feeds Tamper Term Hierarchy plugin requires that terms be searched by term name. Field was skipped. See your mapping settings for '.$element_key.' field in your '.$feeds_source->importer->config[ 'name' ].' importer.', null, WATCHDOG_WARNING, null );
																																																																																$field = [];
																																																																																				return;
																																																																																							}
																																																																																										// load vocabulary
																																																																																													$tax = taxonomy_get_tree( taxonomy_vocabulary_machine_name_load( field_info_field( $mapping[ 'target' ] )[ 'settings' ][ 'allowed_values' ][ 0 ][ 'vocabulary' ] )->vid );
																																																																																																// set auto-create flag
																																																																																																			$autocreate = ( isset( $mapping[ 'autocreate' ] ) && (int) $mapping[ 'autocreate' ] === 1 );
																																																																																																						break;
																																																																																																								}
																																																																																																									}
																																																																																																										
																																																																																																										if ( !is_array( $field ) ) {
																																																																																																													$field = [ $field ];
																																																																																																														}
																																																																																																															
																																																																																																															$terms = [];
																																																																																																																foreach ( $field as $feed_terms ) {
																																																																																																																			// get the term name
																																																																																																																					$term = feeds_tamper_term_hierarchy_fetch_term_name( explode( $settings[ 'separator' ], $feed_terms ), $settings[ 'vocab' ], $tax, $autocreate );
																																																																																																																							if ( $term !== false ) {
																																																																																																																											$terms[] = $term;
																																																																																																																													}
																																																																																																																														}
																																																																																																																															$field = $terms;
																																																																																																																														}

																																																																																																																														/**
																																																																																																																														 * Fetch the term name.
																																																																																																																														  *
																																																																																																																														   * @param array $hierarchy A hierarchy of taxonomy terms in top down order of depth.
																																																																																																																														    * @param int $vid The id of the vocabulary we're using.
																																																																																																																														     * @param array $tax An array of the vocabulary's terms.
																																																																																																																														      * @param bool $autocreate Flag determining whether to create missing terms or not.
																																																																																																																														       * @return mixed The term name or false if term is not found and $autocreate is false.
																																																																																																																														        */
																																																																																																																															function feeds_tamper_term_hierarchy_fetch_term_name( $feed_terms, $vid, &$tax, $autocreate ) {
																																																																																																																																	// get the term id of the deepest term
																																																																																																																																		$termid = feeds_tamper_term_hierarchy_recursively_fetch_term_id( $feed_terms, $vid, $tax, $autocreate );
																																																																																																																																			return ( $termid === false ) ? false : taxonomy_term_load( $termid )->name;
																																																																																																																																		}

																																																																																																																																		/**
																																																																																																																																		 * Recursively fetch the taxonomy term id from the given identifier.
																																																																																																																																		  *
																																																																																																																																		   * @param array $terms_left The remaining terms in order of depth.
																																																																																																																																		    * @param int $vid The vocabulary id.
																																																																																																																																		     * @param array $tax All taxonomy terms of the vocabulary.
																																																																																																																																		      * @param bool $autocreate Flag determining whether to create missing terms or not.
																																																																																																																																		       * @param int $parent The parent term id.
																																																																																																																																		        * @return mixed The term id, or false if term is not found and $autocreate is false.
																																																																																																																																			 */
																																																																																																																																			 function feeds_tamper_term_hierarchy_recursively_fetch_term_id( $terms_left, $vid, &$tax, $autocreate, $parent = 0 ) {
																																																																																																																																				 	// get current term and advance array
																																																																																																																																						$current = array_shift( $terms_left );
																																																																																																																																							// search for current term in given vocabulary
																																																																																																																																								// loops in switch statement because it decreases number of conditional checks
																																																																																																																																									$tid = false;
																																																																																																																																										foreach ( $tax as $term ) {
																																																																																																																																													// found potential match
																																																																																																																																															if ( $term->name == $current ) {
																																																																																																																																																			// when searching terms by name, it is possible to have a match with different parents
																																																																																																																																																						// check to make sure that we do, in fact, have the correct term by ensuring the parents match
																																																																																																																																																									foreach ( $term->parents as $pid ) {
																																																																																																																																																														if ( $pid == $parent ) {
																																																																																																																																																																				// match confirmed.
																																																																																																																																																																									// return term id or descend to next depth
																																																																																																																																																																														return empty( $terms_left ) ? $term->tid : feeds_tamper_term_hierarchy_recursively_fetch_term_id( $tax, $vid, $terms_left, $autocreate, $term->tid );
																																																																																																																																																																																		}
																																																																																																																																																																																					}
																																																																																																																																																																																							}
																																																																																																																																																																																								}
																																																																																																																																																																																									// term wasn't found
																																																																																																																																																																																										if ( $autocreate ) {
																																																																																																																																																																																													// create new term and add it to the vocabulary
																																																																																																																																																																																															$tid = feeds_tamper_term_hierarchy_create_term( $current, $vid, $parent );
																																																																																																																																																																																																	$tax[] = taxonomy_term_load( $tid );
																																																																																																																																																																																																		}
																																																																																																																																																																																																			// if $tid is false OR there are no terms left, return $tid
																																																																																																																																																																																																				// else decend to next depth
																																																																																																																																																																																																					return ( $tid === false || empty( $terms_left ) ) ? $tid : feeds_tamper_term_hierarchy_recursively_fetch_term_id( $tax, $vid, $terms_left, $autocreate, $tid );
																																																																																																																																																																																																				}

																																																																																																																																																																																																				/**
																																																																																																																																																																																																				 * Create new taxonomy term.
																																																																																																																																																																																																				  *
																																																																																																																																																																																																				   * @param string $name The taxonomy term name.
																																																																																																																																																																																																				    * @param int $vid The id of the vocabulary to which this new term will belong.
																																																																																																																																																																																																				     * @param int $parent (Optional) The term id of the parent term. Defaults to 0 for root terms.
																																																																																																																																																																																																				      * @return int The new term's id.
																																																																																																																																																																																																				       */
																																																																																																																																																																																																				       function feeds_tamper_term_hierarchy_create_term( $name, $vid, $parent = 0 ) {
																																																																																																																																																																																																					       	$newterm = new stdClass();
																																																																																																																																																																																																							$newterm->name = $name;
																																																																																																																																																																																																								$newterm->vid = $vid;
																																																																																																																																																																																																									$newterm->parent = $parent;
																																																																																																																																																																																																										taxonomy_term_save( $newterm );
																																																																																																																																																																																																											return $newterm->tid;
																																																																																																																																																																																																										}


